import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import ContainerDucks from './views/container/Ducks';

export default createStore(ContainerDucks, composeWithDevTools(applyMiddleware(thunk)));
