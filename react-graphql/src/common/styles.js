import styled from 'styled-components';

export const FlexColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justityContent};
`;

export const SubTitle = styled.span`
  margin-top: 10px;
  font-size: 16px;
  font-weight: 500;
`;

export const Title = styled.span`
  letter-spacing: ${props => props.spacing && '0.1rem'};
  text-transform: uppercase;
  color: ${props => props.color || '#fff'};
  font-size: 1.3rem;
  font-weight: 500;
`;

FlexColumnContainer.defaultProps = {
  justityContent: 'center',
  alignItems: 'center',
};
