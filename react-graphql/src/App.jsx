import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Container from './views/container';
import Header from './components/Header';
import Footer from './components/Footer';

const App = () => (
  <Provider store={store}>
    <Header />
    <Container />
    <Footer />
  </Provider>
);

export default App;
