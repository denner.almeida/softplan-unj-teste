export const COUNTRIES_LIST = {
  Country: [
    {
      _id: '661',
      name: 'Brazil',
      capital: 'Brasília',
      flag: {
        svgFile: 'https://restcountries.eu/data/bra.svg',
      },
    },
    {
      _id: '860',
      name: 'Canada',
      capital: 'Ottawa',
      flag: {
        svgFile: 'https://restcountries.eu/data/can.svg',
      },
    },
    {
      _id: '2180',
      name: 'Japan',
      capital: 'Tokyo',
      flag: {
        svgFile: 'https://restcountries.eu/data/jpn.svg',
      },
    },
    {
      _id: '4425',
      name: 'United States of America',
      capital: 'Washington, D.C.',
      flag: {
        svgFile: 'https://restcountries.eu/data/usa.svg',
      },
    },
  ],
};
