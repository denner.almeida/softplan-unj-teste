import React from 'react';
import { screen, render, waitForElement } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../App';
import { request } from 'graphql-request';
import { COUNTRIES_LIST } from './helper';

jest.mock('graphql-request');

describe('Testes para o componente App.jsx', () => {
  afterEach(() => jest.resetAllMocks());

  it('Deve renderizar os componentes iniciais', async () => {
    request.mockResolvedValue([]);
    render(<App />);

    const header = screen.getByRole('banner');
    expect(header).toHaveTextContent(/listagem de países/i);

    const formCard = screen.getByText(/filtros/i);
    expect(formCard).toBeInTheDocument();

    const input = screen.getByRole('textbox');
    expect(input.getAttribute('placeholder')).toBe('Digite o nome ou capital do País');

    const button = screen.getByRole('button');
    expect(button).toHaveTextContent(/pesquisar/i);

    const footer = screen.getByRole('contentinfo');
    expect(footer).toBeInTheDocument();
    expect(footer.firstChild).toHaveTextContent('Avaliação Técnica - UNJ | Softplan');
  });

  it('Deve listar os Países quando o componente App for construído', async () => {
    request.mockResolvedValue(COUNTRIES_LIST);
    render(<App />);

    const loading = screen.getByText(/pesquisando registros/i);
    expect(loading).toBeInTheDocument();

    const countriesList = await waitForElement(() => screen.getAllByTestId('country'));
    expect(countriesList).toHaveLength(4);
    const countriesNames = countriesList.map(country => country.textContent);

    expect(countriesNames).toEqual(['Brazil', 'Canada', 'Japan', 'United States of America']);

    expect(request).toHaveBeenCalledTimes(1);
  });

  it('Deve renderizar a informação de "Nenhum registro encontrado" quando não existir itens', async () => {
    request.mockResolvedValue({ Country: [] });
    render(<App />);

    const loading = screen.getByText(/pesquisando registros/i);
    expect(loading).toBeInTheDocument();

    const infoImg = await waitForElement(() => screen.getByAltText('Info'));
    expect(infoImg).toBeInTheDocument();
    expect(infoImg.getAttribute('src')).toContain('error-info.svg');

    const message = screen.getByText(/nenhum registro encontrado/i);
    expect(message).toBeInTheDocument();

    expect(request).toHaveBeenCalledTimes(1);
  });

  it('Deve listar os Países por nome ou capital por filtros', async () => {
    request.mockResolvedValue(COUNTRIES_LIST);
    render(<App />);

    const countriesList = await waitForElement(() => screen.getAllByTestId('country'));
    expect(countriesList).toHaveLength(4);

    request.mockResolvedValue({ Country: [...COUNTRIES_LIST.Country.slice(2)] });

    const input = screen.getByRole('textbox');
    userEvent.type(input, 'mock');

    const button = screen.getByRole('button');
    userEvent.click(button);

    const countriesListFiltered = await waitForElement(() => screen.getAllByTestId('country'));
    expect(countriesListFiltered).toHaveLength(2);

    expect(request).toHaveBeenCalledTimes(2);
  });

  it('Deve bloquear as requisições do filtro quando o valor for igual ao da última busca', () => {
    request.mockResolvedValue(COUNTRIES_LIST);
    render(<App />);

    const input = screen.getByRole('textbox');
    userEvent.type(input, 'mock');

    const button = screen.getByRole('button');
    userEvent.click(button);
    userEvent.click(button);

    userEvent.type(input, 'othermock');
    userEvent.click(button);
    userEvent.click(button);

    expect(request).toHaveBeenCalledTimes(3);
  });

  it('Deve renderizar a informação de "Ocorreu um erro inexperado" quando o erro for capturado', async () => {
    render(<App />);

    request.mockRejectedValue(new Error());

    const infoImg = await waitForElement(() => screen.getByAltText('Info'));
    expect(infoImg).toBeInTheDocument();
    expect(infoImg.getAttribute('src')).toContain('error-info.svg');

    const message = screen.getByText(/ocorreu um erro inexperado/i);
    expect(message).toBeInTheDocument();

    expect(request).toHaveBeenCalledTimes(1);
  });
});
