export const defaultDelays = {
  SHORT: 300,
  MEDIUM: 600,
  HIGH: 900,
  EXTRA_HIGH: 1200,
};

export const COUNTRIES_API_URL = 'https://countries-274616.ew.r.appspot.com';
