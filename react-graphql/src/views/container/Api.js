import { request, gql } from 'graphql-request';
import { COUNTRIES_API_URL } from '../../utils/constants';

const delayEffect = (promise, ms) => new Promise(resolve => setTimeout(() => resolve(promise), ms));

export const getAllCountries = (delay = 0) => {
  const query = gql`
    query getAllCountries {
      Country {
        _id
        name
        capital
        flag {
          svgFile
        }
      }
    }
  `;

  const response = delayEffect(request(COUNTRIES_API_URL, query), delay);

  return response;
};

export const getAllCountriesByNameOrCapital = (value, delay = 0) => {
  const query = gql`
    query getAllCountries($value: String) {
      Country(filter: { OR: [{ name_contains: $value }, { capital_contains: $value }] }) {
        _id
        name
        capital
        flag {
          svgFile
        }
      }
    }
  `;

  const variables = {
    value,
  };

  const response = delayEffect(request(COUNTRIES_API_URL, query, variables), delay);

  return response;
};
