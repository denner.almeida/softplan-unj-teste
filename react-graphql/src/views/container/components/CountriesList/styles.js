import styled from 'styled-components';

export const CountryListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const CountryContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  height: 100%;
  justify-content: center;
`;

export const Countr = styled.img`
  max-width: 250px;
`;
