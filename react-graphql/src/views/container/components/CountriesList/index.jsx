import React from 'react';
import PropTypes from 'prop-types';
import { CountryListContainer, CountryContainer } from './styles';
import Card from '../../../../components/Card';
import { SubTitle } from '../../../../common/styles';

const CountriesList = ({ countries }) => {
  return (
    <CountryListContainer>
      {countries.map(({ _id, name, capital, flag }) => (
        <Card gutterTop="1x" key={_id}>
          <CountryContainer>
            <img src={flag?.svgFile} alt="Flag" width="250px" />
            <SubTitle data-testid="country">{name}</SubTitle>
            <span>Capital: {capital || 'Não informado'}</span>
          </CountryContainer>
        </Card>
      ))}
    </CountryListContainer>
  );
};

CountriesList.propTypes = {
  countries: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CountriesList;
