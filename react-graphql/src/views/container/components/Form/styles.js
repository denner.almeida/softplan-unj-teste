import styled from 'styled-components';

export const Input = styled.input`
  padding: 10px 15px;
  outline: none;
  border: 1px solid #cecece;
  background: #fff;
  border-radius: 2px;
  transition: all 0.2s;
  color: #2d3748;
  flex: 1;

  &:focus {
    border-radius: 4px;
    border-color: #1a4776;
    box-shadow: 0 0 0 0.125em rgba(50, 115, 220, 0.25);
  }
`;

export const Button = styled.button`
  color: #1a4776;
  padding: 5px 10px;
  text-align: center;
  text-transform: uppercase;
  white-space: nowrap;
  border: 2px solid #1a4776;
  background-color: transparent;
  transition: background-color 0.2s, color 0.3s;
  cursor: pointer;
  border-radius: 3px;
  outline: none;
  font-weight: 600;

  &:hover {
    color: #fff;
    background-color: #1a4776;
  }

  &:focus {
    box-shadow: 0 0 0 0.125em rgba(50, 115, 220, 0.25);
  }
`;

export const Form = styled.form`
  margin: 20px 5px;
  display: flex;
  flex-wrap: wrap;
`;
