import React from 'react';
import PropTypes from 'prop-types';
import { Input, Form as StyledForm, Button } from './styles';
import Card from '../../../../components/Card';
import { CardDivider } from '../../../../components/Card/styles';
import { Title } from '../../../../common/styles';

const Form = ({ onSubmit, inputRef }) => {
  return (
    <Card>
      <Title color="#2d3748">Filtros</Title>
      <CardDivider />
      <StyledForm onSubmit={onSubmit}>
        <Input placeholder="Digite o nome ou capital do País" type="text" ref={inputRef} />
        <Button>Pesquisar</Button>
      </StyledForm>
    </Card>
  );
};

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  inputRef: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default Form;
