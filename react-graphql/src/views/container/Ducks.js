import { getAllCountries, getAllCountriesByNameOrCapital } from './Api';
import { defaultDelays } from '../../utils/constants';

const types = {
  FETCH_COUNTRIES: 'FETCH/COUNTRIES',
  FETCH_COUNTRIES_LOADING: 'FETCH/COUNTRIES_LOADING',
  FETCH_COUNTRIES_ERROR: 'FETCH/COUNTRIES_ERROR',
};

const INITIAL_STATE = {
  error: false,
  loading: false,
  countries: [],
};

export const fetchAllCountries = () => async dispatch => {
  try {
    dispatch({ type: types.FETCH_COUNTRIES_LOADING, payload: true });

    const { Country: payload } = await getAllCountries(defaultDelays.MEDIUM);

    dispatch({ type: types.FETCH_COUNTRIES, payload });
  } catch (err) {
    dispatch({ type: types.FETCH_COUNTRIES_ERROR });
  } finally {
    dispatch({ type: types.FETCH_COUNTRIES_LOADING, payload: false });
  }
};

export const fetchAllCountriesByNameOrCapital = value => async dispatch => {
  try {
    dispatch({ type: types.FETCH_COUNTRIES_LOADING, payload: true });

    const { Country: payload } = await getAllCountriesByNameOrCapital(value, defaultDelays.MEDIUM);

    dispatch({ type: types.FETCH_COUNTRIES, payload });
  } catch (err) {
    dispatch({ type: types.FETCH_COUNTRIES_ERROR });
  } finally {
    dispatch({ type: types.FETCH_COUNTRIES_LOADING, payload: false });
  }
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case types.FETCH_COUNTRIES:
      return {
        ...state,
        countries: payload,
      };
    case types.FETCH_COUNTRIES_ERROR:
      return {
        ...state,
        error: true,
      };
    case types.FETCH_COUNTRIES_LOADING:
      return {
        ...state,
        loading: payload,
      };
    default:
      return state;
  }
};
