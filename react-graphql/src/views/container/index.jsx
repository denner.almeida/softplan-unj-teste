import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Main } from './styles';
import Form from './components/Form';
import CountriesList from './components/CountriesList';
import Error from '../../components/Error';
import Loading from '../../components/Loading';
import { fetchAllCountries, fetchAllCountriesByNameOrCapital } from './Ducks';

const Container = () => {
  const { loading, error, countries } = useSelector(state => state);
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const [lastSearchValue, setLastSearchValue] = useState('');

  const handleSubmit = useCallback(
    event => {
      event.preventDefault();
      const value = inputRef.current?.value.trim();

      if (value !== lastSearchValue) {
        dispatch(fetchAllCountriesByNameOrCapital(value));
        setLastSearchValue(value);
      }
    },
    [dispatch, lastSearchValue],
  );

  useEffect(() => {
    dispatch(fetchAllCountries());
  }, [dispatch]);

  const renderContent = () => {
    if (loading) {
      return <Loading />;
    }
    if (error) {
      return <Error />;
    }
    if (!countries || !countries.length) {
      return <Error mensagem="Ops. Nenhum registro encontrado." />;
    }

    return <CountriesList countries={countries} />;
  };

  return (
    <Main>
      <Form onSubmit={handleSubmit} inputRef={inputRef} />
      {renderContent()}
    </Main>
  );
};

export default Container;
