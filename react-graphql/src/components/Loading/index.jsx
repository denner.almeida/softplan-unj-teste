import React from 'react';
import Card from '../Card';
import { FlexColumnContainer, SubTitle } from '../../common/styles';

import SpinnerSvg from '../../assets/spinner.svg';
import { SvgAnimation } from './styles';

const Loading = () => {
  return (
    <Card gutterTop="1x">
      <FlexColumnContainer>
        <SvgAnimation src={SpinnerSvg} width="45px" />
        <SubTitle>Pesquisando Registros...</SubTitle>
      </FlexColumnContainer>
    </Card>
  );
};

export default Loading;
