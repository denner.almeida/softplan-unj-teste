import styled, { keyframes } from 'styled-components';

const SpinnerAnimation = keyframes`
  from { transform: rotate(0deg);}
  to { transform: rotate(360deg);}
`;

export const SvgAnimation = styled.img`
  animation: ${SpinnerAnimation} 1s linear infinite;
`;
