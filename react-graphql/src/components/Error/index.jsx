import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card';
import { FlexColumnContainer, SubTitle } from '../../common/styles';

import ErrorSvg from '../../assets/error-info.svg';

const Error = ({ mensagem }) => {
  return (
    <Card gutterTop="1x">
      <FlexColumnContainer>
        <img src={ErrorSvg} alt="Info" width="150px" />
        <SubTitle>{mensagem}</SubTitle>
      </FlexColumnContainer>
    </Card>
  );
};

Error.defaultProps = {
  mensagem: 'Ocorreu um erro inexperado.',
};

Error.propTypes = {
  mensagem: PropTypes.string,
};

export default Error;
