import styled from 'styled-components';

export const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  background-color: #fff;
  border-radius: 2px;
  padding: 15px 10px;
  position: relative;
  margin-top: ${props => props.marginTop};
  margin-bottom: ${props => props.marginBottom};
  min-width: 350px;
  flex: 1;
  width: ${props => props.witdh};
`;

export const SubTitle = styled.span`
  margin-top: 10px;
  font-size: 16px;
  font-weight: 500;
`;

export const CardDivider = styled.hr`
  border: none;
  height: 1px;
  background-color: rgba(0, 0, 0, 0.2);
  position: absolute;
  left: 0;
  width: 100%;
`;
