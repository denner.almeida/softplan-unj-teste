import React from 'react';
import PropTypes from 'prop-types';
import { Card as StyledCard } from './styles';

const sizes = {
  '1x': '10px',
  '2x': '20px',
  '3x': '30px',
};

const Card = ({ gutterTop, gutterBottom, width, children }) => (
  <StyledCard marginTop={sizes[gutterTop]} marginBottom={sizes[gutterBottom]} width={width}>
    {children}
  </StyledCard>
);

Card.defaultProps = {
  gutterTop: null,
  gutterBottom: null,
  width: null,
};

Card.propTypes = {
  gutterTop: PropTypes.oneOf(['1x', '2x', '3x']),
  gutterBottom: PropTypes.oneOf(['1x', '2x', '3x']),
  width: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default Card;
