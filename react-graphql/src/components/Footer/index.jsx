import React from 'react';
import { Footer } from './styles';

export default () => (
  <Footer>
    <p>Avaliação Técnica - UNJ | Softplan &copy;</p>
  </Footer>
);
