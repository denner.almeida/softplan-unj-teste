import styled from 'styled-components';

export const Footer = styled.footer`
  padding: 10px 30px;
  border-top: 1px solid #cecece;
`;
