import React from 'react';
import { Header, Container } from './styles';
import { Title } from '../../common/styles';

export default () => (
  <Header>
    <Container>
      <Title spacing={1}>Listagem de Países</Title>
    </Container>
  </Header>
);
