import styled from 'styled-components';

export const Header = styled.header`
  min-height: 48px;
  display: flex;
  background-color: #1a4776;
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const Container = styled.div`
  padding: 10px 40px;
  width: 100%;
`;
