# Teste para Desenvolvedor de Software - Fullstack | Softplan (UNJ)

## Tecnologias utilizadas

### Geral

- Docker e Docker-compose
- Nginx
- Gitlab CI

<div style="display: flex; justify-content: space-around">
<img src="https://cdn.svgporn.com/logos/docker-icon.svg" width="30">
<img src="https://cdn.svgporn.com/logos/nginx.svg" width="50">
<img src="https://cdn.svgporn.com/logos/gitlab.svg" width="30">
</div>


### Exercício 1
- React
- Testing-library
- Styled-componets
- Graphql-request
- Eslint, prettier e editorconfig
- PropTypes
- Redux e redux-thunk

<div style="display: flex; justify-content: space-around">
<img src="https://cdn.svgporn.com/logos/react.svg" width="30">
<img src="https://cdn.svgporn.com/logos/graphql.svg" width="25">
</div>

### Exercícios 2 e 3
- Java 11
- Spring boot e Spring Framework
- Lombok

<div style="display: flex; justify-content: space-around">
<img src="https://cdn.svgporn.com/logos/java.svg" width="30">
<img src="https://cdn.svgporn.com/logos/spring.svg" width="30">
</div>

## Implementação

### Exercício 1

- A criação da interface foi feita com o `styled-components` para deixar o visual mais agradável, e também deixar explícito o nível de conhecimento em CSS aos avaliadores.
- Os testes foram realizados apenas para o componente `App.js` e não para cada componente filho.
- As funcionalidades do exercício foram respeitadas, porém existem mais duas funcionalidades a mais criadas:
  1. Permitir a busca não apenas por nome do País, mas também por capital.
  2. Evitar que o usuário faça várias requisições sobre a mesma busca, ou seja, se o usuário manter o filtro e continuar clicando em "Pesquisar" não irá acontecer nada até que o filtro seja diferente.
- Foi adicionado configurações de guia de estilos, padronização e verificação de tipos utilizando `eslist, prettier e prop-types`, tudo configurado do zero e feito como se fosse um projeto real.
- Foi utilizado o padrão `Ducks` para o redux e a utilização do `redux-thunk`.
  
### Exercício 2

- Este exercício foi implementado junto com o `Exercício 3`, e foi criado uma `end-point` para que os avaliadores acessem.
- Foi criado algumas validações no fluxo deste exercício:
  1. O tamanho do disco deve ser maior que 0.
  2. Os tamanhos dos arquivos deve conter pelo menos 1 item.
  3. Os tamanhos dos arquivos não devem ser negativos ou maior que o tamanho do disco informado.
- Foi levado em consideração as regras mensionadas que não deve haver mais de dois arquivos por disco, e nenhum arquivo pode ser fracionado.
Porém na entrada de exemplo do exercício deu a entender que estava com um item a mais ou estava ignorando essas regras. 
- Todos as validações e fluxos estão nos testes unitários.
 
### Exercicio 3

- Este exercício apenas foi implementado as validações:
  1. A taxa não deve ser negativa.
  2. O valor deve ser maior que 0.
- Todos as validações e fluxos estão nos testes unitários.
  
## Acesso Externo

* O projeto foi configurado em uma VPS particular com um servidor web `Nginx`. No arquivo `softplan-teste-nginx.conf` do projeto, está disponível os detalhes da configuração.

### ENDPOINTS

* Exercício 1: `http://162.241.50.151/`

* Exercício 2: `http://162.241.50.151/api/exercicio2/minimum-tape-count`

  1. Método GET
  2. Exemplo de solicitação: `http://162.241.50.151/api/exercicio2/minimum-tape-count?tapeSize=100&fileSizes=100&fileSizes=50&fileSizes=80`

* Exercício 3: `http://162.241.50.151/api/exercicio3/valueWithTaxes`
  1. Método POST
  2. Exemplo de solicitação: 
  ```json
    {
    "tax": 3.5,
    "amount": 20000
    }
  ```
