package br.com.softplan.exercicio2.controller;

import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BatchController.class)
@ExtendWith(SpringExtension.class)
@DisplayName("Testes para o controller: 'BatchController'")
public class BatchControllerTest {

    private static final String ENDPOINT = "/exercicio2/minimum-tape-count";

    @Autowired
    private MockMvc mvc;

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar exceção quando validar os campos na solicitação")
    public void deveRetornarExcecao_quandoOsCamposNaSolicitacaoEstiveremIncorretos() {
        mvc.perform(get(ENDPOINT)
            .param("tapeSize", "0")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", Matchers
                .containsInAnyOrder("O campo 'fileSizes' não deve estar vazio",
                    "O campo 'tapeSize' deve ser maior que 0")));

        mvc.perform(get(ENDPOINT)
            .param("tapeSize", "100")
            .param("fileSizes", "100", "101", "-1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$", Matchers.is("O(s) tamanho(s): 101, -1, estão inválidos.")));
    }

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar a quantidade mínima de discos")
    public void deveRetornarAQuantidadeMinimaDeDiscos() {
        mvc.perform(get(ENDPOINT)
            .param("tapeSize", "100")
            .param("fileSizes", "20", "30", "10", "15")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", Matchers.is(2)));
    }
}
