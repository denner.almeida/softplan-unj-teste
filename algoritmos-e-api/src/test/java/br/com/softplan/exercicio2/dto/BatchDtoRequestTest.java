package br.com.softplan.exercicio2.dto;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.validation.ValidationException;

import static br.com.softplan.Helpers.umBatchDtoRequest;

@DisplayName("Testes para a DTO: 'BatchDtoRequest'")
public class BatchDtoRequestTest {

    @Test
    @DisplayName("Deve retornar exceção quando existir valores inválidos no tamanho dos arquivos")
    public void deveRetornarExcecao_quandoExistirValoresInvalidosNoTamanhoDosArquivos() {
        Assertions.assertThatExceptionOfType(ValidationException.class)
            .isThrownBy(umBatchDtoRequest(100, 10, -1, 101, 50, 90)::validarTamanhoArquivos)
            .withMessageContaining("O(s) tamanho(s): -1, 101, estão inválidos");

        Assertions.assertThatExceptionOfType(ValidationException.class)
            .isThrownBy(umBatchDtoRequest(200, 200, -200, 201)::validarTamanhoArquivos)
            .withMessageContaining("O(s) tamanho(s): -200, 201, estão inválidos.");
    }

    @Test
    @DisplayName("Deve retornar a quantidade mínima de discos")
    public void deveRetornarAQuantidadeMinimaDeDiscos() {
        Assertions.assertThat(umBatchDtoRequest(50, 50).getMinimumTapeCount())
            .isEqualTo(1);

        Assertions.assertThat(umBatchDtoRequest(100, 100, 0, 0, 100).getMinimumTapeCount())
            .isEqualTo(2);

        Assertions.assertThat(umBatchDtoRequest(100, 90, 80, 30, 50).getMinimumTapeCount())
            .isEqualTo(3);

        Assertions.assertThat(umBatchDtoRequest(100, 100, 100, 99, 2).getMinimumTapeCount())
            .isEqualTo(4);

        Assertions.assertThat(umBatchDtoRequest(100, 100, 100, 50, 50, 49, 51).getMinimumTapeCount())
            .isEqualTo(4);

        Assertions.assertThat(umBatchDtoRequest(500, 50, 50, 150, 500).getMinimumTapeCount())
            .isEqualTo(3);
    }
}
