package br.com.softplan.exercicio3.controller;

import br.com.softplan.exercicio3.dto.TaxaDtoRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static br.com.softplan.Helpers.umaTaxaDtoRequest;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TaxaController.class)
@ExtendWith(SpringExtension.class)
@DisplayName("Testes para o controller: 'TaxaController'")
public class TaxaControllerTest {

    private static final String ENDPOINT = "/exercicio3/valueWithTaxes";
    private static ObjectMapper mapper;

    @Autowired
    private MockMvc mvc;

    @BeforeAll
    static void init() {
        mapper = new ObjectMapper();
    }

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar exceção de validação quando não existir valores na requisição")
    public void deveRetornarExcecaoDeValidacao_quandoNaoExistirValoresNaSolicitacao() {
        mvc.perform(post(ENDPOINT)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsBytes(new TaxaDtoRequest())))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", Matchers
                .containsInAnyOrder("O campo 'tax' não deve ser nulo", "O campo 'amount' não deve ser nulo")));
    }

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar exceção de validação quando os valores forem negativos na requisição")
    public void deveRetornarExcecaoDeValidacao_quandoOsValoresForemNegativos() {
        mvc.perform(post(ENDPOINT)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsBytes(umaTaxaDtoRequest(-1F, -123.0F))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[*]", Matchers
                .containsInAnyOrder("O campo 'amount' deve ser maior que 0",
                    "O campo 'tax' deve ser maior ou igual a 0")));
    }

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar o valor original quando somente a taxa for zero")
    public void deveRetornarOValorOriginal_quandoSomenteATaxaForZero() {
        mvc.perform(post(ENDPOINT)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsBytes(umaTaxaDtoRequest(0, 326.55F))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.valueWithTaxes", Matchers.is(326.55)));
    }

    @Test
    @SneakyThrows
    @DisplayName("Deve retornar o valor original quando somente a taxa for zero")
    public void deveRetornarOCalculoDoValorComATaxa() {
        mvc.perform(post(ENDPOINT)
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsBytes(umaTaxaDtoRequest(2.2F, 10000F))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.valueWithTaxes", Matchers.is(10220.00)));
    }
}
