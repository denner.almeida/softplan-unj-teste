package br.com.softplan.exercicio3.dto;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static br.com.softplan.Helpers.umaTaxaDtoRequest;

@DisplayName("Testes para a DTO: 'TaxaDtoResponse'")
public class TaxaDtoResponseTest {

    @Test
    @DisplayName("Deve retornar o cálculo do valor com as taxa")
    public void deveRetornarOCalculoDoValorComATaxa() {
        Assertions.assertThat(TaxaDtoResponse.of(umaTaxaDtoRequest(2.33F, 2899.47F)))
            .extracting(TaxaDtoResponse::getValueWithTaxes)
            .extracting(BigDecimal::floatValue)
            .isEqualTo(2967.03F);
    }

    @Test
    @DisplayName("Deve retornar o valor original quando a taxa for zero")
    public void deveRetornarOValorOriginal_quandoATaxaForZero() {
        Assertions.assertThat(TaxaDtoResponse.of(umaTaxaDtoRequest(0, 570.55F)))
            .extracting(TaxaDtoResponse::getValueWithTaxes)
            .extracting(BigDecimal::floatValue)
            .isEqualTo(570.55F);
    }
}
