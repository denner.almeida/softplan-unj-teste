package br.com.softplan;

import br.com.softplan.exercicio2.dto.BatchDtoRequest;
import br.com.softplan.exercicio3.dto.TaxaDtoRequest;

import java.math.BigDecimal;

public class Helpers {

    public static TaxaDtoRequest umaTaxaDtoRequest(float tax, float amount) {
        return new TaxaDtoRequest(BigDecimal.valueOf(tax), BigDecimal.valueOf(amount));
    }

    public static BatchDtoRequest umBatchDtoRequest(int tapeSize, int... fileSizes) {
        return new BatchDtoRequest(fileSizes, tapeSize);
    }
}
