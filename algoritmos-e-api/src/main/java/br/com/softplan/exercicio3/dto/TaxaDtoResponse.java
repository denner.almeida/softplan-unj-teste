package br.com.softplan.exercicio3.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.MathContext.UNLIMITED;

@Data
@Builder
@AllArgsConstructor
public class TaxaDtoResponse {

    private static final BigDecimal CEM = BigDecimal.valueOf(100);
    private static final Integer ESCALA = 2;

    private BigDecimal valueWithTaxes;

    public static TaxaDtoResponse of(TaxaDtoRequest taxaDtoRequest) {
        return TaxaDtoResponse.builder()
            .valueWithTaxes(
                taxaDtoRequest.getAmount()
                    .multiply(taxaDtoRequest.getTax()
                        .divide(CEM, UNLIMITED)
                        .add(BigDecimal.ONE))
                    .setScale(ESCALA, RoundingMode.HALF_EVEN))
            .build();
    }
}
