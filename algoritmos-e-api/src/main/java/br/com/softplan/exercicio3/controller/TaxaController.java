package br.com.softplan.exercicio3.controller;

import br.com.softplan.exercicio3.dto.TaxaDtoRequest;
import br.com.softplan.exercicio3.dto.TaxaDtoResponse;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("exercicio3")
public class TaxaController {

    @PostMapping("valueWithTaxes")
    public TaxaDtoResponse calculateTaxes(@Validated @RequestBody TaxaDtoRequest taxaDtoRequest) {
        return TaxaDtoResponse.of(taxaDtoRequest);
    }
}
