package br.com.softplan.exercicio3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaxaDtoRequest {

    @NotNull
    @PositiveOrZero
    private BigDecimal tax;
    @NotNull
    @Positive
    private BigDecimal amount;
}
