package br.com.softplan.exercicio2.dto;

import br.com.softplan.exercicio2.interfaces.Batch;
import br.com.softplan.exercicio2.interfaces.BatchResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.util.ObjectUtils;

import javax.validation.ValidationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
@AllArgsConstructor
public class BatchDtoRequest implements Batch {

    @NotEmpty
    private int[] fileSizes;
    @Positive
    private int tapeSize;

    public void validarTamanhoArquivos() {
        var tamanhosInvalidos = IntStream.of(fileSizes)
            .boxed()
            .filter(fileSize -> fileSize < 0 || fileSize > tapeSize)
            .map(String::valueOf)
            .collect(Collectors.toList());

        if (!ObjectUtils.isEmpty(tamanhosInvalidos)) {
            throw new ValidationException(String.format("O(s) tamanho(s): %s, estão inválidos.",
                String.join(", ", tamanhosInvalidos)));
        }
    }

    private BatchResponse toBatchResponse() {
        return batch -> {
            var resto = new AtomicInteger();
            return IntStream.of(fileSizes).reduce(0, (acc, size) -> {
                if (size <= resto.get()) {
                    resto.set(0);
                } else {
                    resto.set(tapeSize - size);
                    ++acc;
                }

                return acc;
            });
        };
    }

    public int getMinimumTapeCount() {
        return toBatchResponse().getMinimumTapeCount(this);
    }
}
