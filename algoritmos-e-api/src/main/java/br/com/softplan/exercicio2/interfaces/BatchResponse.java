package br.com.softplan.exercicio2.interfaces;

@FunctionalInterface
public interface BatchResponse {

    int getMinimumTapeCount(final Batch batch);
}
