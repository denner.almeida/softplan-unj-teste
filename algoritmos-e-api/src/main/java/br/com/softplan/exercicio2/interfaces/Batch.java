package br.com.softplan.exercicio2.interfaces;

public interface Batch {
    int[] getFileSizes();

    int getTapeSize();
}
