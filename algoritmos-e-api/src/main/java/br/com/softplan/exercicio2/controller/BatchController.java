package br.com.softplan.exercicio2.controller;

import br.com.softplan.exercicio2.dto.BatchDtoRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("exercicio2")
public class BatchController {

    @GetMapping("minimum-tape-count")
    public int getMinimumTapeCount(@Validated BatchDtoRequest batch) {
        batch.validarTamanhoArquivos();

        return batch.getMinimumTapeCount();
    }
}
